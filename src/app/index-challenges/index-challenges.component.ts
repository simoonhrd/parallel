import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder } from '@angular/forms';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-index-challenges',
  templateUrl: './index-challenges.component.html',
  styleUrls: ['./index-challenges.component.scss']
})
export class IndexChallengesComponent implements OnInit {

  challengeForm: any;

  challenge : any;

  constructor(
    private db: AngularFirestore,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getAllChallenge()
  }
  
  showAddChallenge : boolean = false;
  showEditChallenge : boolean = false;
  

  ShowAddChallenge() {
    this.showAddChallenge = true;
  }

  HideAddChallenge() {
    this.showAddChallenge = false;
  }
  ShowEditChallenge(challenge: any) {
    this.showEditChallenge = true;
    this.challenge = challenge;
  }

  HideEditChallenge() {
    this.showEditChallenge = false;
  }

  challenges: any = null;
  
  async getAllChallenge() {
  
    const challengeCollection = await this.db.collection('challenges');
    const getChallenges = await challengeCollection.snapshotChanges().pipe(
      map(actions => actions.map((a: any) => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

    getChallenges.subscribe((res: any) => {
      this.challenges = res;
    });
  }
  
  DeleteChallenge(challengeId: any) {
    this.db
     .collection("challenges")
     .doc(challengeId)
     .delete();

  }
}
