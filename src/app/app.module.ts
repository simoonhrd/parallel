import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexSourcesComponent} from './index-sources/index-sources.component';
import { HomePageComponent } from './home-page/home-page.component';
import { IndexChallengesComponent } from './index-challenges/index-challenges.component';
import { IndexStatsComponent } from './index-stats/index-stats.component';
import { AProposComponent } from './a-propos/a-propos.component';
import { IndexAddSourceComponent } from './index-add-source/index-add-source.component';
import { IndexAddChallengeComponent } from './index-add-challenge/index-add-challenge.component';
import { AngularFireModule } from "@angular/fire/compat";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { firebaseConfig } from 'environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { IndexEditChallengeComponent } from './index-edit-challenge/index-edit-challenge.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IndexSourcesComponent,
    HomePageComponent,
    IndexChallengesComponent,
    IndexStatsComponent,
    AProposComponent,
    IndexAddSourceComponent,
    IndexAddChallengeComponent,
    IndexEditChallengeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
