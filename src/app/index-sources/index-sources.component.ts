import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-sources',
  templateUrl: './index-sources.component.html',
  styleUrls: ['./index-sources.component.scss']
})
export class IndexSourcesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  showAddSource: boolean = false;

  ShowAddSource() {
    this.showAddSource = true;
  }

  HideAddSource() {
    this.showAddSource = false;
  }
}
