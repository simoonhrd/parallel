import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { async } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-index-add-challenge',
  templateUrl: './index-add-challenge.component.html',
  styleUrls: ['./index-add-challenge.component.scss']
})
export class IndexAddChallengeComponent implements OnInit {

  addChallengeForm: any;
  
  constructor(
    private db: AngularFirestore,
    private fb: FormBuilder
    ) {
  }

  ngOnInit(): void {
    this.generateCreateForm();
  }

  @Output() hideAddChallenge = new EventEmitter();

  async generateCreateForm(): Promise<void> {
    this.addChallengeForm = this.fb.group({
      title: [''],
      category: [''],
      description: ['']
    });
  }

  onSubmit():void{
    this.db
    .collection("challenges")
    .add(this.addChallengeForm.value)
    this.HideAddChallenge()
  }

  HideAddChallenge() {
    this.hideAddChallenge.emit();
  }

}
