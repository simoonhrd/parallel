import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-index-edit-challenge',
  templateUrl: './index-edit-challenge.component.html',
  styleUrls: ['./index-edit-challenge.component.scss']
})
export class IndexEditChallengeComponent implements OnInit {

  editChallengeForm: any;

  constructor(
    private db: AngularFirestore,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.generateUpdateForm();
  }

  @Output() hideEditChallenge = new EventEmitter();
  @Input() challenge: any;

  async generateUpdateForm(): Promise<void> {
    this.editChallengeForm = this.fb.group({
      title: [this.challenge.title],
      category: [this.challenge.category],
      description: [this.challenge.description]
    });
  }

  onSubmit():void{
    this.db
		.collection('challenges')
		.doc(this.challenge.id)			
    .update(this.editChallengeForm.value);
    this.HideEditChallenge()
  }

  HideEditChallenge() {
    this.hideEditChallenge.emit();
  }
}
