import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-index-add-source',
  templateUrl: './index-add-source.component.html',
  styleUrls: ['./index-add-source.component.scss']
})
export class IndexAddSourceComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Output() hideAddSource = new EventEmitter();

  HideAddSource() {
    this.hideAddSource.emit();
  }
}
