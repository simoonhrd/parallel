import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { IndexChallengesComponent } from './index-challenges/index-challenges.component';
import { AProposComponent } from './a-propos/a-propos.component';
import { IndexAddChallengeComponent } from "./index-add-challenge/index-add-challenge.component";



const routes: Routes = [
  // { path: '', component: HomePageComponent}
  { path: 'dashboard', component: HomePageComponent},
  { path: 'challenges', component: IndexChallengesComponent},
  { path: 'apropos', component: AProposComponent},
  { path: 'addchallenge', component: IndexAddChallengeComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
